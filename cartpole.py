import gym
from gym import *
from decimal import Decimal

def main():
    env = gym.make('CartPole-v1') 
    obschg = {0: [0,0,0,0,0], 1:[0,0,0,0,0] }
    failedobs = [[[0,0],[0,0]],[[0,0],[0,0]],[[0,0],[0,0]],[[0,0],[0,0]]]
    goodobs = [[0,0], [0,0], [0,0], [0,0]]
    focusobs = {
        (0, ) : 0,
        (1, ) : 0,
        (2, ) : 0,
        (3, ) : 0,
        (0, 1) : 0,
        (0, 2) : 0,
        (0, 3) : 0,
        (1, 2) : 0,
        (1, 3) : 0,
        (2, 3) : 0,
        (0, 1, 2) : 0,
        (0, 1, 3) : 0,
        (1, 2, 3) : 0,
        (0, 1, 2, 3) : 0,
    }
    testState = True
    focusTests = list(focusobs.keys())
    currentTest = focusTests.pop()
    maxFocuses = []
    absolutereward = 0
    lastreward = 0
    for episodes in range(1,1001):
        lobs = env.reset()
        totalreward = 0
        done = False
        successobs = [[],[],[],[]]
        while not done:
            try:
                predictions = {0:{'outcome':{}, 'success':[]}, 1:{'outcome':{}, 'success':[]}}
                for x in currentTest:
                    for y in range(2):
                        predictions[y]['outcome'][x] = (lobs[x] + (obschg[y][x]/obschg[y][4]))
                        prediction = predictions[y]['outcome'][x]
                        posFail = abs(prediction - (failedobs[x][0][0]/failedobs[x][0][1]))
                        negFail = abs(prediction - (failedobs[x][1][0]/failedobs[x][1][1]))
                        unlikelyToFail = min([posFail, negFail])
                        predictions[y]['success'].append(unlikelyToFail)

                        nearBestObs = abs(prediction - (goodobs[x][0]/goodobs[x][1]))
                        predictions[y]['success'].append(-1*nearBestObs)
                for y in range(2):
                    predictions[y]['success'] = sum(predictions[y]['success'])
                if predictions[0]['success'] > predictions[1]['success']:
                    action = 0
                elif predictions[0]['success'] < predictions[1]['success']:
                    action = 1
                else:
                    action = env.action_space.sample()
            except ZeroDivisionError:
                action = env.action_space.sample()
            obs, reward, done, info = env.step(action)
            #if episodes > 900:
                #env.render()
            totalreward+=reward
            temp = [ obschg[action][x] + (obs[x] - lobs[x]) for x in range(4) ]
            temp.append(obschg[action][4]+1)
            if not done:
                for x in range(4):
                    successobs[x].append(obs[x])
            obschg[action] = temp
            lobs = obs
        if totalreward < 200:
            for x in range(4):
                if obs[x] < 0:
                    failedobs[x][0][0]+=obs[x]
                    failedobs[x][0][1]+=1
                else:
                    failedobs[x][1][0]+=obs[x]
                    failedobs[x][1][1]+=1
        if totalreward > 50:
            for x in range(4):
                temp = successobs[x][0:round(len(successobs)/2)]
                goodobs[x][0]+=sum(temp)
                goodobs[x][1]+=len(temp)
        if testState:
            if focusTests:
                focusobs[currentTest] = totalreward
                currentTest = focusTests.pop()
            else:
                focusList = list(focusobs.values())
                foundFocus = list(focusobs.keys())[focusList.index(max(focusList))]
                maxFocuses.append(foundFocus)
                currentTest = foundFocus
                testState = False
        absolutereward+=totalreward
        if episodes > 300 and len(focusobs) > 3:
            for focus in list(focusobs.keys()):
                if focusobs[focus] < 10:
                    del(focusobs[focus])
        if episodes % 100 == 0:
            absoluteAverage = absolutereward/100
            print(absoluteAverage)
            if lastreward > absoluteAverage or absoluteAverage < 150:
                focusTests = list(focusobs.keys())
                currentTest = focusTests.pop()
                testState = True
            lastreward = absolutereward
            absolutereward = 0

main()
